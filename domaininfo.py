from __future__ import print_function

# import os.path

import requests
import json
import pandas as pd

from env import *


# get domain info for a list of plesk servers

def plesk_server_doms(srvr, u, p):
    # server vars
    # variablize the server name and url
    server_name = f'{srvr}.metisentry.net'
    server_url = f"https://{server_name}:8443/api/v2/domains"

    # api request vars
    payload = {'name': ''}
    plesk_req = requests.get(server_url, params=payload, auth=(u, p))
    domain_info = json.loads(plesk_req.text)

    # populate lists
    domain_list = [domain['name'] for domain in domain_info]
    id_list = [domain['id'] for domain in domain_info]
    created_list = [domain['created'] for domain in domain_info]

    # populate dict for consumption
    domain_dict = {"Domain": domain_list, "ID": id_list, "Created": created_list}

    return domain_dict

def create_csv(srvr, u, p):
    # create csv file
    df = pd.DataFrame(plesk_server_doms(srvr, u, p))
    file_name = srvr + '_domains.csv'

    return df.to_csv(file_name)

def main():
    servers = [kirk, spock, ems1]
    for server in servers:
        svr_name = server['name']
        u = server['plesk_user']
        p = server['plesk_passwd']
        create_csv(svr_name, u, p)

if __name__ == "__main__":
    main()