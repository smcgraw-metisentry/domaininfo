# Domain Info Script

This script pulls domain information from Plesk servers via the Plesk REST API and generates a CSV file with that info. The info collected at this time is:

1. Domain Name
2. Date Created
3. ID number

This might change and additional information might be added in future versions.

Relying on humans to keep spreadsheets up to date is less than reliable and being the human updating spreadsheets is awful, as it is tedious and boring. So, the goal is to automate the gathering of domain information for Plesk servers so that we know what domains are on what servers without hoping that a spreadsheet is up-to-date and accurate.
## Installation
You will need to create a Python virtual environment: ```python3 -m venv venv```
Activate the virtual environment: ```source venv/bin/activate```
Install the required libraries: ```pip install <library>```
So far:
- requests
- pandas

Rename `example-env.py` to `env.py` and add dictionaries for any servers added.

## To run the script 
Once the venv is activated and libs installed, simply type ```python3 domaininfo.py``` to generate a CSV of domians for each of the servers specified.

## Future Plans
I might make this a command-line application, perhaps using the `click` library, so that a server name can be passed as an argument. I would also like to eventually be able to update a Google Docs spreadsheet with the info, wich could be automated to run periodically.
